import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueApexCharts from 'vue-apexcharts';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

Vue.use(VueApexCharts);
Vue.use(ElementUI);
// Vue.component('apexchart', VueApexCharts);

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
