import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import axios from 'axios';

Vue.use(Vuex);

const triggerCommit = (commit, formData, actionName) => new Promise((resolve) => {
  commit('toggleIsLoading', true);
  const { algorithm, param } = formData;
  const requestData = {};
  if (algorithm.withParam) {
    requestData[algorithm.paramName] = param;
  }
  requestData.values = formData.values.map((el) => +el.y);

  axios.post(`/calculate/${algorithm.url}`, requestData).then((response) => {
    const newChart = JSON.parse(JSON.stringify(formData));
    newChart.values = newChart.values
      .map(({ x, y }, index) => ({ x, y, smooth: response.data.values[index] }));
    commit(actionName, newChart);
    commit('setSelectedChart', newChart);
    commit('toggleIsLoading', false);
    resolve();
  }).catch(() => {
    commit('toggleIsLoading', false);
  });
});

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

export default new Vuex.Store({
  state: {
    charts: [],
    isCreateDrawerShow: false,
    isEditDrawerShow: false,
    selectedChart: null,
    isLoading: false,
  },
  mutations: {
    toggleIsCreateDrawerShow(state, value) {
      state.isCreateDrawerShow = value;
    },
    toggleIsEditDrawerShow(state, value) {
      state.isEditDrawerShow = value;
    },
    toggleIsLoading(state, value) {
      state.isLoading = value;
    },
    addChart(state, chart) {
      state.charts = [...state.charts, chart];
    },
    updateChart(state, updatedChart) {
      state.charts = state.charts.map((chart) => {
        if (chart.id === updatedChart.id) {
          return updatedChart;
        }
        return chart;
      });
    },
    deleteChart(state, chartToDelete) {
      if (state.selectedChart && state.selectedChart.id === chartToDelete.id) {
        state.selectedChart = null;
      }
      state.charts = state.charts.filter((chart) => chart.id !== chartToDelete.id);
    },
    setSelectedChart(state, chart) {
      state.selectedChart = chart;
    },
  },
  actions: {
    createChart({ commit }, formData) {
      return triggerCommit(commit, formData, 'addChart');
    },
    updateChart({ commit }, formData) {
      return triggerCommit(commit, formData, 'updateChart');
    },
  },
  plugins: [vuexLocal.plugin],
});
